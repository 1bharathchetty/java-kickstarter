package lesson01;

import java.util.Scanner; // Scanner is the utility you need to read inputs for the program
// Read more about Scanner API here : https://docs.oracle.com/javase/8/docs/api/java/util/Scanner.html

public class InteractiveGreeting {
	
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Create new object of class Scanner, based on System.in and call it input
        Scanner input = new Scanner(System.in);
        System.out.print("Enter your name:");                
        String yourName = input.nextLine();
        // say hello
        System.out.println("Hello " + yourName + "!!");
        input.close();
    }

}
