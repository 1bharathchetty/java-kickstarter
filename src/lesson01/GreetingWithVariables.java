package lesson01;

public class GreetingWithVariables {

	public static void main(String[] args) {
        String name;    // Allocate some memory, refer to that variable it as name
        name = "World";  // Set memory of variable- name with the value - World
        System.out.println("Hello, "+ name + "!!"); // say hello
	}

}
