package lesson01;

public class Greeting {
	
    /**
     * main() method executes when Java program is run.
     * 
     * public = accessible to all
     * static = does not need to be declared to be used
     * void = returns nothing
     */

	/**
     * @param args the command line arguments
     */
	public static void main(String[] args) {
        // this is a comment
        System.out.println("Hello, World !");
        String who = "World";
        System.out.printf("\nHello, %s !", who);


	}

}
