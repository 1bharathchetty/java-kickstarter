package lesson04;

import java.util.Random;

public class RandomClassExample {
	
    public static void main(String[] args) {
        // Demonstrates use of an existing class
        Random generator = new Random();  
        // Random() will use a random "seed"        
        int i = generator.nextInt(100);      // pick a random number between 0 and 99
        System.out.println(i);
        i = generator.nextInt(100);
        System.out.println(i);
        i = generator.nextInt(100);
        System.out.println(i);    
    }

}
